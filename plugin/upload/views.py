from django.shortcuts import render, get_object_or_404
from rest_framework.views   import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Upload_banner
from .forms import BannerForm
from .serializers import Upload_bannerSerializer

# Create your views here.


def index (request):
    return render(request, 'index.html')

def goto (request):
    return render(request, 'Upload_banner.html')



def upload_banner(request):
    form = BannerForm(request.POST, request.FILES)

    if request.method == 'POST':
        if form.is_valid():
            _type = form.save(commit=False)
            _type.document_file = request.FILES['image_file']


            try:
                _type.save()


                returnmsg = {"status_code": "You must be awesome, the download has been completed successfully ", }
            except Exception as e:
                returnmsg = {"wrong": "Oops , An error occurred, you need to"
                                      ""
                                      " something wrong ", }

            return render(request, 'upload_status.html', returnmsg)
    return render(request, 'banner_upload.html', {   'form': form
    })
class Banner_list(APIView):
    def get(self,request):
        banners=Upload_banner.objects.all()
        serializer=Upload_bannerSerializer(banners ,many=True)
        return Response(serializer.data)