from rest_framework import serializers
from .models import Upload_banner

from rest_framework import viewsets



class   Upload_bannerSerializer(serializers.ModelSerializer):
    class Meta:
        model=Upload_banner
        fields='__all__'