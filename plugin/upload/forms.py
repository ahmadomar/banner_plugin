from django import forms
from .models import Upload_banner



class BannerForm(forms.ModelForm):
    class Meta:
        model = Upload_banner
        fields = ('title', 'publisher','image_file' )

