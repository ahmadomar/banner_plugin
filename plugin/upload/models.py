from django.db import models

# Create your models here.
class Upload_banner(models.Model):



    title = models.CharField(max_length=100,blank=True , null =True)
    publisher=models.CharField(max_length=100,blank=True , null =True)

    image_file = models.FileField(default='')
    uploaded_at = models.DateTimeField(editable=True, auto_now_add=True)

    def __str__(self):
        return self.title

