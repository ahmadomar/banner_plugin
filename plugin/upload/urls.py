from django.conf import settings
from django.conf.urls import url
from django.urls import path
from difflib import SequenceMatcher
from django.conf.urls.static import static
from . import views
from django.urls import include, path

urlpatterns = [
    path('', views.index, name='index'),
    path('Upload_banner', views.goto, name='goto'),
    path('upload', views.upload_banner, name='upload_banner'),

]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)